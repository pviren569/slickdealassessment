//
//  CredentialHelper.swift
//  sd-ios-test
//
//  Created by Patel, Virenkumar on 11/20/19.
//  Copyright © 2019 Slickdeals, LLC. All rights reserved.
//

import Foundation


class CredentialsHelper: NSObject {
    class func sharedInstance() -> CredentialsHelper {
        struct Instance {
            static let credentialsHelper = CredentialsHelper()
        }
        return Instance.credentialsHelper
    }
    
    func baseUrl() -> String {
        return "https://slickdeas-api-test.s3-us-west-2.amazonaws.com"
    }
    
    func getdeals_frontPage() -> String {
        return baseUrl() + "/get_deals_frontpage.json"
    }
    
    // For Future Use to pass default Parameters to request
    func sdDefaultParams() -> [String:String] {
        
//        var params : [String:String] = [:]
//        return params
        return [:]
    }
}
