//
//  extension+Array.swift
//  sd-ios-test
//
//  Created by Patel, Virenkumar on 11/20/19.
//  Copyright © 2019 Slickdeals, LLC. All rights reserved.
//

import Foundation

extension Array where Element == DealSummary {
    mutating func priceHighToLow() -> [DealSummary] {
        return self.sorted { (deal1, deal2) -> Bool in
            return deal2.price ?? 0.0 < deal1.price ?? 0.0
        }
    }
    
    mutating func priceLowToHigh() -> [DealSummary] {
        return self.sorted { (deal1, deal2) -> Bool in
            
            return deal2.price ?? 0.0 > deal1.price ?? 0.0
        }
    }
    
    mutating func scoreHighToLow() -> [DealSummary] {
        return self.sorted { (deal1, deal2) -> Bool in
            let deal1Score = deal1.voteCount + (deal1.commentCount * 8)
            let deal2Score = deal2.voteCount + (deal2.commentCount * 8)
            return deal2Score < deal1Score
        }
    }
    
    mutating func scoreLowToHigh() -> [DealSummary] {
           return self.sorted { (deal1, deal2) -> Bool in
               let deal1Score = deal1.voteCount + (deal1.commentCount * 8)
               let deal2Score = deal2.voteCount + (deal2.commentCount * 8)
               return deal2Score > deal1Score
           }
    }
    
    mutating func filterByPriceRange(min: Double?, max: Double?)-> [DealSummary] {
        return self.filter({
            if let minValue = min, let maxValue = max, let price = $0.price, minValue < maxValue {
                return minValue ... maxValue ~= price
            } else if let minValue = min, let price = $0.price, max == nil {
                return price > minValue
            } else if let maxValue = max, let price = $0.price, min == nil {
                return price < maxValue
            } else {
                return true
            }
        })
        
    }
}
