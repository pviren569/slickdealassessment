//
//  ViewController.swift
//  sd-ios-test
//
//  Created by Fritz Ammon on 9/25/19.
//  Copyright © 2019 Slickdeals, LLC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var deals: [DealSummary] = []
    var dealsToDisplay: [DealSummary] = [] {
        didSet {
            DispatchQueue.main.async {
                self.title = "\(self.dealsToDisplay.count) deals"
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
    }
    var dealDAO: DealSummaryResultsProtocol?
    
    var refreshControl = UIRefreshControl()
    
//MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Register the cell from nib
        tableView.register(UINib(nibName: "DealTableViewCell", bundle: nil), forCellReuseIdentifier: "DealCell")
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        // fetch the deals
        // Passing default DealDAO to fetch the data..
        // During writing unit test cases we will pass Mock DAO to fetch the data for stubs, Checkout test cases for more details....
        self.fetchDeals(dataAccessObject: dealDAO ?? DealDAO())
    }
    
//MARK: - DAO functions
    func fetchDeals(dataAccessObject: DealSummaryResultsProtocol){
        dataAccessObject.dealSummaryResults { (deals) in
            self.deals = deals
            // If user has their setting stored in userDefaults them rearrange the deal by their prefrences.
            self.reArragedealWithUserSettings(dataAccessObject: self.dealDAO ?? DealDAO())
        }
    }
    
    func reArragedealWithUserSettings(dataAccessObject: DealSummaryResultsProtocol) {
        if let sortType = dataAccessObject.getUserSettings() {
            if sortType.priceHighToLow {
                self.dealsToDisplay = self.deals.priceHighToLow()
            } else if sortType.priceLowToHigh {
                self.dealsToDisplay = self.deals.priceLowToHigh()
            } else if sortType.scoreHighToLow {
                self.dealsToDisplay = self.deals.scoreHighToLow()
            } else if sortType.scoreLowToHigh {
                self.dealsToDisplay = self.deals.scoreLowToHigh()
            } else {
                self.dealsToDisplay = self.deals
            }
        } else {
            self.dealsToDisplay = self.deals
        }
    }
    
    func saveUserSettings(filterTye: SortType?, dataAccessObject: DealSummaryResultsProtocol) {
        dataAccessObject.saveUserSettings(filterType: filterTye)
    }
    
    func getUserSettings(dataAccessObject: DealSummaryResultsProtocol) -> SortType? {
        return dataAccessObject.getUserSettings()
    }
    
//MARK: - UIAlertController
    func presentActionSheet() {
        // Creating Alert Controller
        let alertController = UIAlertController(title: nil, message: "[Sort the deal way you like]", preferredStyle: .actionSheet)
        
        // Creating different actions
        let priceHighToLowAction = UIAlertAction(title: "Price - High to Low", style: .default) { (_) in
            self.dealsToDisplay = self.dealsToDisplay.priceHighToLow()
            let filterType = SortType(priceHighToLow: true, priceLowToHigh: false, scoreHighToLow: false, scoreLowToHigh: false)
            self.saveUserSettings(filterTye: filterType, dataAccessObject: self.dealDAO ?? DealDAO())
            
        }
        
        let priceLowToHighAction = UIAlertAction(title: "Price - Low to High", style: .default) { (_) in
            self.dealsToDisplay = self.dealsToDisplay.priceLowToHigh()
            let filterType = SortType(priceHighToLow: false, priceLowToHigh: true, scoreHighToLow: false, scoreLowToHigh: false)
            self.saveUserSettings(filterTye: filterType, dataAccessObject: self.dealDAO ?? DealDAO())
        }
        
        let scoreHighToLowAction = UIAlertAction(title: "Score - High To Low", style: .default) { (_) in
            self.dealsToDisplay = self.dealsToDisplay.scoreHighToLow()
            let filterType = SortType(priceHighToLow: false, priceLowToHigh: false, scoreHighToLow: true, scoreLowToHigh: false)
            self.saveUserSettings(filterTye: filterType, dataAccessObject: self.dealDAO ?? DealDAO())
        }
        
        let scoreLowToHighAction = UIAlertAction(title: "Score - Low To High", style: .default) { (_) in
            self.dealsToDisplay = self.dealsToDisplay.scoreLowToHigh()
            let filterType = SortType(priceHighToLow: false, priceLowToHigh: false, scoreHighToLow: false, scoreLowToHigh: true)
            self.saveUserSettings(filterTye: filterType, dataAccessObject: self.dealDAO ?? DealDAO())
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        // Adding actions to alertController
        alertController.addAction(priceHighToLowAction)
        alertController.addAction(priceLowToHighAction)
        alertController.addAction(scoreHighToLowAction)
        alertController.addAction(scoreLowToHighAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func presentAlert() {
        let alertController = UIAlertController(title: nil, message: "[Enter the Range]", preferredStyle: .alert)
        alertController.addTextField { (minTextField) in
            minTextField.placeholder = "Min."
            minTextField.keyboardType = .numberPad
        }
        alertController.addTextField { (maxTextField) in
            maxTextField.placeholder = "Max."
            maxTextField.keyboardType = .numberPad
        }
        
        let clearAction = UIAlertAction(title: "Clear", style: .destructive) { (_) in
            self.dealsToDisplay = self.deals
        }
        let filterAction = UIAlertAction(title: "Filter", style: .default) { (_) in
            if alertController.textFields?.count == 2 {
                guard let minTextFiled = alertController.textFields?[0], let maxTextField = alertController.textFields?[1] else{
                    return
                }
                
                var min: Double?
                var max: Double?
                if let minText = minTextFiled.text {
                    min = Double(minText)
                }
                if let maxText = maxTextField.text {
                    max = Double(maxText)
                }
                
                self.dealsToDisplay = self.deals.filterByPriceRange(min: min, max: max)
            }
        }
        
        alertController.addAction(clearAction)
        alertController.addAction(filterAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
//MARK: - IBActions
    @objc func refresh(sender:Any) {
        self.fetchDeals(dataAccessObject: dealDAO ?? DealDAO())
    }
    
    @IBAction func sortAction(_ sender: Any) {
        self.presentActionSheet()
    }
    
    @IBAction func filterAction(_ sender: Any) {
        self.presentAlert()
    }
    
    @IBAction func clearFilter(_ sender: Any) {
        self.dealsToDisplay = self.deals
        // Removing user's setting from UserDefauls
        self.saveUserSettings(filterTye: nil, dataAccessObject: self.dealDAO ?? DealDAO())
    }
}

//MARK: - UITableViewDelegates
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dealsToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DealCell", for: indexPath) as? DealCell else {
            return UITableViewCell()
        }
        cell.configure(dealSummary: dealsToDisplay[indexPath.row])
        return cell
    }
}

