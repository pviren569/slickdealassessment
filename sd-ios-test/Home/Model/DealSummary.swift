//
//  DealSummary.swift
//  sd-ios-test
//
//  Created by Patel, Virenkumar on 11/20/19.
//  Copyright © 2019 Slickdeals, LLC. All rights reserved.
//

import Foundation

/*
 {
   "id": 335291,
   "title": "BIC Flex 5 Razor",
   "price": "$1",
   "extra": "+ Free Shipping",
   "up_votes": 53,
   "comments": 30,
   "image": "https://static.slickdealscdn.com/attachment/5/1/4/8/2/1/3/200x200/6199087.thumb?=.jpeg",
   "store_name": "Frys",
   "show_flag": true,
   "flag_text": "NEW",
   "flag_text_color": "FFFFFF",
   "flag_background_color": "FF6320"
 }
 */

struct DealSummary:Codable {
    var price: Double? = nil
    var title: String
    var extra: String
    var voteCount: Int
    var commentCount: Int
    var imageLink: String
    // Check init(from decoder: Decoder) method for explanation of why we have this variable
    var hardCodedPrice: String? = nil
    
    enum CodingKeys: String, CodingKey {
        case voteCount = "up_votes"
        case commentCount = "comments"
        case imageLink = "image"
        case price, title, extra
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        extra = try container.decode(String.self, forKey: .extra)
        voteCount = try container.decode(Int.self, forKey: .voteCount)
        commentCount = try container.decode(Int.self, forKey: .commentCount)
        imageLink = try container.decode(String.self, forKey: .imageLink)
        
        // Had to go this route because price was coming as string and converting the string to Double wasn't possible everytime(For example sometimes it comes as "Free" or "4 for $10")  I didn't had certain rules about how we wanted to display on UI, so created a new property "hardCodedPrice" to store this kind of values
        // So decode value to temp String, try convert it to Double, if it's possible store it in price(Double) otherwise store it in hardCodedPrice(String)
        let priceStr = try container.decode(String.self, forKey: .price)
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        if let number = formatter.number(from: priceStr) {
            let amount = number.doubleValue
            self.price = amount
        } else {
            hardCodedPrice = priceStr
        }
    }
}

struct SortType: Codable {
    let priceHighToLow: Bool
    let priceLowToHigh: Bool
    let scoreHighToLow: Bool
    let scoreLowToHigh: Bool
    let minPrice:Double? = nil
    let maxPrice: Double? = nil
}
