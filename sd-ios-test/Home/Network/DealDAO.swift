//
//  DealDAO.swift
//  sd-ios-test
//
//  Created by Patel, Virenkumar on 11/20/19.
//  Copyright © 2019 Slickdeals, LLC. All rights reserved.
//

import Foundation


typealias DealsSummaryResponse = ([DealSummary]) -> Void

protocol DealSummaryResultsProtocol {
    func dealSummaryResults(completion: @escaping DealsSummaryResponse)
    func saveUserSettings(filterType: SortType?)
    func getUserSettings() -> SortType?
}

struct DealDAO: DealSummaryResultsProtocol {
    let defaults = UserDefaults.standard
    
    func dealSummaryResults(completion: @escaping DealsSummaryResponse) {
        let url = CredentialsHelper.sharedInstance().getdeals_frontPage()
        
        let request = RequestConfig(type: .Data, method: .Get, apiEndpoint: url, params: nil, httpBody: nil, headers: [:], background: true)
        
        SDequest(request) { (data, response, error) in
            if let error = error {
                print("DealDAO:: ERROR  Unable to complete request on endpoint: \(url) \nError: \(error)")
                completion([])
            }
            
            guard let dealsData = data else {
                completion([])
                return
            }
            do {
                 let deals = try JSONDecoder().decode([DealSummary].self, from: dealsData)
                completion(deals)
            } catch {
                print("DealDAO:: Unable to decode dealSummary Response. Error is \(error)")
                completion([])
            }
        }
    }
    
    func saveUserSettings(filterType: SortType?) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(filterType) {
            defaults.set(encoded, forKey: "FilterType")
        }
    }
    
    func getUserSettings() -> SortType? {
        if let filteTypeobject = defaults.object(forKey: "FilterType") as? Data {
            let decoder = JSONDecoder()
            if let filterType = try? decoder.decode(SortType.self, from: filteTypeobject ) {
                return filterType
            }
        }
        return nil
    }
}
