//
//  DealCell.swift
//  sd-ios-test
//
//  Created by Patel, Virenkumar on 11/20/19.
//  Copyright © 2019 Slickdeals, LLC. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class DealCell: UITableViewCell {
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var extraLabel: UILabel!
    @IBOutlet weak var votesLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var dealImage: UIImageView!
    
    func configure(dealSummary: DealSummary) {
        if let price = dealSummary.price {
            priceLabel.text = "$\(price)"
        } else {
            priceLabel.text = dealSummary.hardCodedPrice ?? "Price Not Available"
        }
        nameLabel.text = "\(dealSummary.title)"
        extraLabel.text = "\(dealSummary.extra)"
        votesLabel.text = "\(dealSummary.voteCount)"
        commentsLabel.text = "\(dealSummary.commentCount)"
        dealImage.sd_setImage(with: URL(string: dealSummary.imageLink), placeholderImage: UIImage(named: "icn_image_coming_soon"), options: [], context: nil)
    }
}
