# README #

                    **************Task**************

1) As a user I should be able to get dynamic deals from a backend service delivered through json. This data can be expected to change between requests, and I would expect my deal list to update accordingly.
            a. This json file can be found here: https://slickdeas-api-test.s3-us-west-2.amazonaws.com/get_deals_frontpage.json
            
            Task1 : Create SDRequest class for more abstraction over URLSession: 10 min(because I have already done it for my personal use. I was just leveraging  this code)
            Task2 : modify current model to make compatible with API response: 30 min
            Task2 : Create DAO to get API response: 30 min
            Task3 : hook up viewcontroller with DAO to fetch the model: 60 min
            Task4 : write Unit test case: 30 min
            TotalSize: 2 hour
            
2) As a user I should see an image of the deal in the same container as the deal name and price. 

            Task1 : Implement pod "SDWebImage" to get images from different url(really powerful framwork to work with when too many images need to be download and shown to UI) 15 min
            Task2 : incorporate "SDWebImage" framework to display images in TableViewCell: 10 min
            TotalSize:: 30 min
3) As a user I should be able to sort the deal feed using various criteria
        a. By Price
            ■ Highest to Lowest
            ■ Lowest to Highest
        b. By Score (highest to lowest) using the following heuristics:
            ● 1 up-vote = 1 point
            ● 1 comment = 8 points
                ■ Example: A deal with 10 up-votes and 2 comments would receive a score of 26.
                
            Task1 : Create UIActionsheet to for ux to get input from user: 30 mins
            Task2 : Create Extension for Array of "Deals" to get the sorted collection: 30 mins
            Task3 : Write unit test cases: 15 mins
            Totalsize :: 1:30 hour
            
4) As a user I should be able to filter the deal feed by Price Range by specifying a minimum and maximum value.
                
            Task1 : Create UIAlert to get input from user: 30 min
            Task2 : create extension of array to get range collections : 15 min
            Task3 : Write unit test cases: 15 min
            Totalsize: 1 hour
            
5) As a user I should be able to restart the app and maintain my last filter choice, including any values that may have been put into fields.

            Task1 : implement 2 methods in DealDAO to save and get user's setting(will be using UserDefaults to data consistency) : 30 min
            Task2 : Create function in ViewController to check if user has saved setting or not and make some code refactor based on that: 30 min
            Task2 : Write unit test cases: 15 min
            Totalsize: 1:30 hours
 6) As a user I should be able to rotate my phone into landscape mode and the app should automatically adjust to a landscape layout.
 
            Task1 : while creating ViewController in storyboard make sure to use proper AutoLayout 

7) As a user I should be able to see the total number of deals displayed in the deal feed and this number should automatically update when the filter changes.
                
            Task1: User property Observers to finish this story

8) As a user I expect that the data is coming from a remote endpoint and could change dynamically between requests.
                
            Task1: User property Observers to finish this story
                
                
                    ********************************Logs********************************
                    
1) Total Time Spent:  Around 7 Hours
    * Make sure current model is compatible with API response, if not make it compatible that way so we don't  to make too much code change 
    * Create DAO to fetch the data, and parse the data to model
    * Use AutoLayout to make app compatible in both rotate and regular mode
    * Write unit test cases
    * Restructure some the code or file for better understanding of the app..
    
    

            ********************************Questions********************************
            
            
1) Did you finish all the work? If not, why?
                
                - Yes
2) Did you make any changes to the provided code? If so, what?

            - I have some changes in "DealSummary" model, because model that we had in current code wasn't compatible with Ap response(For Example price was coming as String and sometimes it wasn't possible to convert it to Double)
            - Changes the file structure of the app for better understanding and code separation.
                
3) How would you add additional sorting orders based on the feedback given by the product team? 

        - Create a function to get the soring orders in Array extension so it can be reusable throughout the app, if we need a new sorting order in same Controller then we can get user's input from some UI element and call the array extension's \function to get the ne value.
        
4) Provide a Retrospective with the following information:
    a. What do you think you did well?
                
        - Some base code was already there, so didn't had to create a new project and start from the scratch
        - Some of the user stories were well written and self explanatory
        
    b. What do you think you did not do well?
    
        - I felt I needed little more explanation in sorting deals by price because some deal's price were like "Free", "4 for 10$", "80k points". That's why I wasn't sure how to compare these kind of deals with other deals
        
    c. If you were to take this test a second time, what would you do differently?
    
        -I would like to rewrite the code in MVVM design pattern, code was in MVC design pattern so I followed that route, but I would love to convert the code in MVVM for better code separation.
        
5) Do you feel this test accurately evaluated your abilities? Why or why not?

        - I believe these are the things this test has evaluated my abilities
            * Ability to work with exiting code
            * How do I divide the user stories into different task and size them(Scrum skill)
            * How do I approach to the new problem
            * My coding standard
            * How much of the code I can reuse from existing code
            * How do create a code that can be reusable for some other purpose
                


