//
//  DealMockDAO.swift
//  sd-ios-testTests
//
//  Created by Patel, Virenkumar on 11/21/19.
//  Copyright © 2019 Slickdeals, LLC. All rights reserved.
//

import Foundation
@testable import sd_ios_test

struct DealMockDAO: DealSummaryResultsProtocol{
    let defaults = UserDefaults.standard
    func dealSummaryResults(completion: @escaping DealsSummaryResponse) {
        guard let dealSummaryData = Utility().getMockData(forResource: "DealSummary") else {
            completion([])
            return
        }
        
        do {
            let jsonDecoder = JSONDecoder()
            
            //decode mock into data structure
            let deals = try jsonDecoder.decode([DealSummary].self, from: dealSummaryData)
            completion(deals)
            
        } catch  {
            completion([])
        }
    }
    
    func saveUserSettings(filterType: SortType?) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(filterType) {
            defaults.set(encoded, forKey: "FilterTypeTest")
        }
    }
    
    func getUserSettings() -> SortType? {
       if let filteTypeobject = defaults.object(forKey: "FilterTypeTest") as? Data {
           let decoder = JSONDecoder()
           if let filterType = try? decoder.decode(SortType.self, from: filteTypeobject ) {
               return filterType
           }
       }
       return nil
    }
    
    
}
