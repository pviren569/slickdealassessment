//
//  HomeViewCotrollertest.swift
//  sd-ios-testTests
//
//  Created by Patel, Virenkumar on 11/21/19.
//  Copyright © 2019 Slickdeals, LLC. All rights reserved.
//

import XCTest
@testable import sd_ios_test

class HomeViewCotrollertest: XCTestCase {

    var viewController: ViewController?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        viewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController
        viewController?.dealDAO = DealMockDAO()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testHomeViewController() {
        if let vc = self.viewController, let _ = vc.view {
            vc.fetchDeals(dataAccessObject: DealMockDAO())
            XCTAssertEqual(23, vc.deals.count, "Deals count didn't match")
            vc.dealsToDisplay = vc.deals.priceHighToLow()
            XCTAssertEqual(vc.dealsToDisplay.count, 23, "After sorting for score high to low dealsToDisplay count is not same")
            XCTAssertEqual(vc.dealsToDisplay[0].price,
                           729.0, "Highest Price did not match")
            vc.dealsToDisplay = vc.deals.priceLowToHigh()
            XCTAssertEqual(vc.dealsToDisplay[22].price,
            729.0, "Highest Price did not match")
            
            vc.dealsToDisplay = vc.deals.scoreHighToLow()
            XCTAssertEqual(vc.dealsToDisplay[0].hardCodedPrice, "80K Points", "Hight Score did not match")
            
            vc.dealsToDisplay = vc.deals.scoreLowToHigh()
            XCTAssertEqual(vc.dealsToDisplay[22].hardCodedPrice, "80K Points", "Hight Score did not match")
            
            vc.dealsToDisplay = vc.deals.filterByPriceRange(min: 2.0, max: 50.0)
            XCTAssertEqual(vc.dealsToDisplay.count, 14 , "deadss count did not match")
            
            
            vc.reArragedealWithUserSettings(dataAccessObject: DealMockDAO())
            
        }
    }
}
